variable "password" {
  type    = string
  default = null
}

variable "username" {
  type    = string
  default = null 
}

variable "kickstart" {
  type    = string
  default = null 
}

variable "iso_file" {
  type    = string
  default = null 
}

variable "vm_id" {
  type    = string
  default = null 
}

variable "template_description" {
  type    = string
  default = null 
}

variable "template_name" {
  type    = string
  default = null 
}

variable "source_name" {
  type    = string
  default = null 
}

source "proxmox-iso" "rhel" {
  boot_command = ["<up><tab>ip=dhcp inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/${var.kickstart}<enter>"]
  boot_wait    = "10s"
  cpu_type = "x86-64-v2-AES"
  disks {
    disk_size         = "80G"
    storage_pool      = "local-lvm"
    type              = "virtio"
  }
  efi_config {
    efi_storage_pool  = "local-lvm"
    efi_type          = "4m"
    pre_enrolled_keys = true
  }
  http_directory           = "http"
  insecure_skip_tls_verify = true
  iso_file                 = "local:iso/${var.iso_file}"
  network_adapters {
    bridge = "vmbr0"
    model  = "virtio"
  }
#  network_adapters {
#    bridge = "vmbr1"
#    model  = "vmxnet3"
#  }
#  network_adapters {
#    bridge = "vmbr3"
#    model  = "vmxnet3"
#  }
#  network_adapters {
#    bridge = "vmbr4"
#    model  = "vmxnet3"
#  }
#  network_adapters {
#    bridge = "vmbr5"
#    model  = "vmxnet3"
#  }
#  network_adapters {
#    bridge = "vmbr6"
#    model  = "vmxnet3"
#  }
  node                 = "proxmox-01"
  vm_id		       = "${var.vm_id}" 
  cores                = "4"
  cloud_init           = true
  cloud_init_storage_pool = "local-lvm"
  memory               = "8192"
  password             = "${var.password}"
  proxmox_url          = "https://192.168.122.180:8006/api2/json"
  ssh_password         = "2fudge"
  ssh_timeout          = "60m"
  ssh_username         = "root"
  template_description = "${var.template_description} ${timestamp()}"
  template_name        = "${var.template_name}"
  unmount_iso          = true
  username             = "${var.username}"
}

build {
  sources = ["source.proxmox-iso.rhel"]

  provisioner "ansible" {
    #extra_arguments = [ "--scp-extra-args", "'-O'" ]
    playbook_file = "./base/main.yml"
  }

}

